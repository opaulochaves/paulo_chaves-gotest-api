package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
)

func GoogleSignin(w http.ResponseWriter, r *http.Request) {
	// TODO make sure this handler only gets called by the frontend
	// maybe a secret key to exchange with frontend when starting the app
	setCors(w) // TODO write a middleware to add CORS
	var googleForm GoogleForm
	var user User
	var err error

	decoder := json.NewDecoder(io.LimitReader(r.Body, appBodySize))
	if err := decoder.Decode(&googleForm); err != nil {
		errorResponse(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = googleForm.Validate(r)
	if err != nil {
		errorResponse(w, err, http.StatusUnprocessableEntity)
		return
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + googleForm.Token)
	if err != nil || response.StatusCode != http.StatusOK {
		errorResponse(w, err, http.StatusUnauthorized)
		return
	}
	defer response.Body.Close()

	var userInfo map[string]interface{}
	contents, _ := ioutil.ReadAll(response.Body)

	err = json.Unmarshal(contents, &userInfo)

	email := userInfo["email"].(string)
	fullame := userInfo["name"].(string)

	if email == "" {
		errorResponse(w, "Email is not available", http.StatusUnauthorized)
		return
	}

	//TODO check email is not empty
	db := GetDB().Where("Email = ?", email).First(&user)
	if !db.RecordNotFound() {
		// if user already exists, update the code
		db = db.Model(&user).Updates(User{
			Google: googleForm.Code,
		})
		if db.Error != nil {
			errorResponse(w, db.Error, http.StatusInternalServerError)
			return
		}
	} else {
		// if not found, create a new user
		newUser := User{
			Email:    email,
			Fullname: fullame,
			Google:   googleForm.Code,
		}
		db = GetDB().Create(&newUser)
		if db.Error != nil {
			errorResponse(w, db.Error, http.StatusInternalServerError)
			return
		}
		user = newUser
	}

	var res []byte
	var token string

	token, err = GenerateJwtToken(&user)
	if err == nil {
		resMap := map[string]interface{}{
			"token": token,
		}
		res, err = json.Marshal(resMap)
	}

	if err != nil {
		errorResponse(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}
