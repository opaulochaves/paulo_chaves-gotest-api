package main

import (
	"encoding/json"
	"io"
	"net/http"
	"time"
)

// signinHandler handle the login
func signinHandler(w http.ResponseWriter, r *http.Request) {
	setCors(w) // TODO write a middleware to add CORS

	var loginForm = LoginForm{}
	var user User
	var err error

	decoder := json.NewDecoder(io.LimitReader(r.Body, appBodySize))
	if err := decoder.Decode(&loginForm); err != nil {
		errorResponse(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = loginForm.Validate(r)
	if err != nil {
		errorResponse(w, err, http.StatusUnprocessableEntity)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	db := GetDB().Where("Email = ?", loginForm.Email).First(&user)

	if db.RecordNotFound() {
		errorResponse(w, "Email or password invalid", http.StatusUnauthorized)
		return
	}

	err = ComparePassword(user.Password, loginForm.Password)
	if err != nil {
		errorResponse(w, "Email or password invalid", http.StatusUnauthorized)
		return
	}

	var token string

	token, err = GenerateJwtToken(&user)
	if err != nil {
		errorResponse(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	resMap := map[string]interface{}{
		"token": token,
	}

	if err = json.NewEncoder(w).Encode(resMap); err != nil {
		panic(err)
	}
}

// signupHandler handle the user registration
func signupHandler(w http.ResponseWriter, r *http.Request) {
	setCors(w) // TODO create middleware
	var signupForm = SignupForm{}
	var newUser User

	decoder := json.NewDecoder(io.LimitReader(r.Body, appBodySize))
	if err := decoder.Decode(&signupForm); err != nil {
		errorResponse(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = signupForm.Validate(r)
	if err != nil {
		errorResponse(w, err, http.StatusUnprocessableEntity)
		return
	}

	db := GetDB().Where("Email = ?", signupForm.Email).First(&newUser)
	if !db.RecordNotFound() {
		errorResponse(w, "Email already taken", http.StatusUnauthorized)
		return
	}

	newUser = User{
		Email:    signupForm.Email,
		Password: signupForm.Password,
	}

	hash, err := HashPassword(newUser.Password)
	if err != nil {
		errorResponse(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	newUser.Password = hash

	db = GetDB().Create(&newUser)
	err = db.Error
	var res []byte
	var token string

	if err == nil {
		token, err = GenerateJwtToken(&newUser)
		if err == nil {
			resMap := map[string]interface{}{
				"token": token,
			}
			res, err = json.Marshal(resMap)
		}
	}
	if err != nil {
		errorResponse(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(res)
}

func forgotPasswordHandler(w http.ResponseWriter, r *http.Request) {
	setCors(w) // TODO write a middleware to add CORS
	var input map[string]interface{}
	var user User

	decoder := json.NewDecoder(io.LimitReader(r.Body, appBodySize))
	if err := decoder.Decode(&input); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// TODO If the email does not exist, display an error message to the user that the email does not exist
	db := GetDB().Where("Email = ?", input["email"]).First(&user)

	if db.RecordNotFound() {
		// TODO in production send a friendly message. "email or password are invalid"
		http.Error(w, db.Error.Error(), 404)
		return
	}

	// set the token to expire in 24 hours
	now := time.Now().AddDate(0, 0, 1)
	token, err := GenerateToken(user.Email)

	if err != nil {
		http.Error(w, db.Error.Error(), 500)
		return
	}

	// TODO update user with token and expire date
	db.Model(&user).Updates(User{
		PasswordResetToken:   token,
		PasswordResetExpires: &now,
	})

	if db.Error != nil {
		http.Error(w, db.Error.Error(), 500)
		return
	}

	// TODO config email SMTP Gmail; send email
	// resetLink := "http://localhost:3000?password_reset?token=" + token
	// body := "To reset the password access <a href=\"" + resetLink + "\">This link.</a>"
	// err = SendEmail([]string{user.Email}, "Reset Password", body)

	// if err != nil {
	// 	http.Error(w, err.Error(), 500)
	// 	return
	// }

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message":"Email sent"}`))
}

func resetPasswordHandler(w http.ResponseWriter, r *http.Request) {
	setCors(w) // TODO write a middleware to add CORS
	var input map[string]interface{}
	var user User

	decoder := json.NewDecoder(io.LimitReader(r.Body, appBodySize))

	// TODO validation must have email and password fields
	if err := decoder.Decode(&input); err != nil {
		// TODO improve error handling. Return a json {message: ""}
		// If it is production, return a generic message
		http.Error(w, err.Error(), 500)
		return
	}

	resetToken := input["reset_token"].(string)
	email := input["email"].(string)
	password := input["password"].(string)
	// TODO add field confirm_password that has to match password

	db := GetDB().Where("email = ? and password_reset_token = ? and now() <= password_reset_expires", email, resetToken).First(&user)
	if db.RecordNotFound() {
		// TODO in production send a friendly message. "email or password are invalid"
		http.Error(w, db.Error.Error(), 404)
		return
	}

	hash, err := HashPassword(password)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	db.Model(&user).Updates(map[string]interface{}{
		"password":               hash,
		"password_reset_expires": nil,
		"password_reset_token":   nil,
	})

	err = db.Error
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message": "Password successfully reseted"}`))
}
