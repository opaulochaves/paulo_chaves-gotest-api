package main

import (
	"fmt"
	"net/http"
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type InputValidation interface {
	Validate(r *http.Request) error
}

// SignupForm - is our implementation of InputValidation and the structure we will
// deserialize into
type SignupForm struct {
	Email           string `json:"email"`
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirm_password"`
}

type LoginForm struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type ProfileForm struct {
	ID              int    `json:"id"`
	Email           string `json:"email"`
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirm_password"`
	Fullname        string `json:"fullname"`
	Address         string `json:"address"`
	Telephone       string `json:"telephone"`
}

type GoogleForm struct {
	Code  string `json:"code"`
	Token string `json:"token"`
}

func (t SignupForm) Validate(r *http.Request) error {
	matchPassword := regexp.MustCompile(fmt.Sprintf("^%s$", t.Password))

	return validation.ValidateStruct(&t,
		// Email cannot be empty
		validation.Field(&t.Email, validation.Required, is.Email),
		/* Password cannot be empty, and must contain only letters and digits, and the length must between 6 and 30 */
		validation.Field(&t.Password, validation.Required, validation.Length(6, 30), is.Alphanumeric),
		validation.Field(&t.ConfirmPassword, validation.Required, validation.Match(matchPassword)),
	)
}

func (t LoginForm) Validate(r *http.Request) error {
	return validation.ValidateStruct(&t,
		// Email cannot be empty
		validation.Field(&t.Email, validation.Required, is.Email),
		/* Password cannot be empty */
		validation.Field(&t.Password, validation.Required),
	)
}

func (t ProfileForm) Validate(r *http.Request) error {
	matchPassword := regexp.MustCompile(fmt.Sprintf("^%s$", t.Password))

	return validation.ValidateStruct(&t,
		validation.Field(&t.ID, validation.Required, validation.Min(1)),
		validation.Field(&t.Email, validation.Required, is.Email),
		validation.Field(&t.Password, validation.Length(6, 30), is.Alphanumeric),
		validation.Field(&t.ConfirmPassword, validation.Match(matchPassword)),
		validation.Field(&t.Telephone, validation.Required, validation.Match(regexp.MustCompile("^[0-9]{3}-[0-9]{3}-[0-9]{4}$"))),
		validation.Field(&t.Address, validation.Required, validation.Length(0, 70)),
		validation.Field(&t.Fullname, validation.Required, validation.Length(0, 70)),
	)
}

func (t GoogleForm) Validate(r *http.Request) error {
	return validation.ValidateStruct(&t,
		validation.Field(&t.Code, validation.Required),
		validation.Field(&t.Token, validation.Required),
	)
}
