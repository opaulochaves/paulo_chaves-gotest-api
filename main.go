package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

// global flags
var appENV string
var appPORT int
var appDBDSN string
var appFrontendURL string
var appTimeout int
var appBodySize int64

// token
var appJWTSecret string
var appJWTExpires int

// email
var appEmailSender string
var appEmailHost string
var appEmailPort string
var appEmailPassword string

// used for CORS preflight checks
func corsHandler(w http.ResponseWriter, r *http.Request) {
	setCors(w)
}

func setCors(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", appFrontendURL)
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, OPTIONS, POST, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
}

func setEnvVars() {
	if os.Getenv("APP_ENV") != "" {
		appENV = os.Getenv("APP_ENV")
	}
	if os.Getenv("PORT") != "" {
		appPORT, _ = strconv.Atoi(os.Getenv("PORT"))
	}
	if os.Getenv("DATABASE_URL") != "" {
		appDBDSN = os.Getenv("DATABASE_URL")
	}
	if os.Getenv("FRONTEND_URL") != "" {
		appFrontendURL = os.Getenv("FRONTEND_URL")
	}
	if os.Getenv("JWT_SECRET") != "" {
		appJWTSecret = os.Getenv("JWT_SECRET")
	}
}

func main() {
	defer CloseDB()

	flag.StringVar(&appENV, "env", "development", "Set run mode of the app. production or development")
	flag.IntVar(&appPORT, "port", 8080, "Specify the port to listen to.")
	flag.StringVar(&appDBDSN, "dbdsn", "root:root@tcp(db:3306)/gotest?charset=utf8&parseTime=True&loc=Local", "The URL to connect to the database")
	flag.StringVar(&appFrontendURL, "frontend-url", "http://localhost:3000", "Base absolute path to the frontend.")
	flag.IntVar(&appTimeout, "timeout", 15, "Seconds for timeout. Good practice: enforce timeouts for servers you create!")
	flag.Int64Var(&appBodySize, "body-size", 1048576, "Limit the size of the body in the request. Default to 1048576 bytes")
	flag.StringVar(&appJWTSecret, "jwt-secret", "s3cr3t", "The secret key to use to sign JWT tokens.")
	flag.IntVar(&appJWTExpires, "jwt-expires", 24, "Number of hours in which the token should expire.")
	flag.StringVar(&appEmailSender, "email-sender", "your-email@email.test", "")
	flag.StringVar(&appEmailHost, "email-host", "smtp.gmail.com", "")
	flag.StringVar(&appEmailPassword, "email-password", "sup3rS3cr3t", "")
	flag.StringVar(&appEmailPort, "email-port", "465", "")
	flag.Parse()

	setEnvVars()

	router := mux.NewRouter()
	api := router.PathPrefix("/api").Subrouter()
	jwtMiddleware := JwtMiddleware()

	api.HandleFunc("/auth/signin", signinHandler).Methods("POST")
	api.HandleFunc("/auth/signup", signupHandler).Methods("POST")
	api.HandleFunc("/auth/forgot", forgotPasswordHandler).Methods("POST")
	api.HandleFunc("/auth/reset", resetPasswordHandler).Methods("POST")
	api.HandleFunc("/auth/google", GoogleSignin).Methods("POST")
	api.Handle("/users/profile", jwtMiddleware.Handler(http.HandlerFunc(GetProfile))).Methods("GET")
	api.Handle("/users/profile", jwtMiddleware.Handler(http.HandlerFunc(SaveProfile))).Methods("PUT")
	api.HandleFunc("/*", corsHandler).Methods("OPTIONS")

	// add database
	_, err := InitDB()
	if err != nil {
		log.Println("connection to DB failed, aborting...")
		log.Fatal(err)
	}

	log.Println("connected to DB")

	srv := &http.Server{
		Handler:      router,
		Addr:         fmt.Sprintf(":%d", appPORT),
		WriteTimeout: time.Duration(appTimeout) * time.Second,
		ReadTimeout:  time.Duration(appTimeout) * time.Second,
	}

	if appENV == "production" {
		log.Println("Running REST API in production mode on port", appPORT)
	} else {
		log.Println("Running REST API in dev mode on port", appPORT)
	}

	if err := srv.ListenAndServe(); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
