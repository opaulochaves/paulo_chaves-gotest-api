package main

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"net/http"
	"time"

	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	jwt "github.com/dgrijalva/jwt-go"

	"golang.org/x/crypto/bcrypt"
)

// GenerateToken returns a unique token based on the provided email string
// See: https://stackoverflow.com/questions/45267125/how-to-generate-unique-random-alphanumeric-tokens-in-golang
func GenerateToken(email string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(email), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
		return "", err
	}

	return base64.StdEncoding.EncodeToString(hash), nil
}

func HashPassword(rawPassword string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(rawPassword), bcrypt.DefaultCost)
	if err == nil {
		return string(hash), err
	}
	return "", err
}

func ComparePassword(hashPassword, rawPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hashPassword), []byte(rawPassword))
	return err
}

func GenerateJwtToken(user *User) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	claims["sub"] = user.ID
	claims["email"] = user.Email
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(appJWTExpires)).Unix()

	return token.SignedString([]byte(appJWTSecret))
}

func JwtMiddleware() *jwtmiddleware.JWTMiddleware {
	return jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(appJWTSecret), nil
		},
		SigningMethod: jwt.SigningMethodHS256,
	})
}

func errorResponse(w http.ResponseWriter, msg interface{}, status int) {
	data, err := json.Marshal(map[string]interface{}{
		"status": status,
		"errors": msg,
	})
	if err != nil {
		log.Println("Failed marshaling", err)
	} else {
		log.Println(status, msg)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(data)
}
