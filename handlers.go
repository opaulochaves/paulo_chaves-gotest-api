package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
)

func GetProfile(w http.ResponseWriter, r *http.Request) {
	var profile User

	user := r.Context().Value("user")
	claims := user.(*jwt.Token).Claims.(jwt.MapClaims)
	email := claims["email"].(string)

	db := GetDB().Where("Email = ?", email).First(&profile)
	if db.RecordNotFound() {
		errorResponse(w, "Profile not found", http.StatusNotFound)
		return
	}

	data, _ := json.Marshal(map[string]interface{}{
		"id":        profile.ID,
		"email":     profile.Email,
		"fullname":  profile.Fullname,
		"address":   profile.Address,
		"telephone": profile.Telephone,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
}

// SaveProfile updates the profile of the logged user
func SaveProfile(w http.ResponseWriter, r *http.Request) {
	setCors(w) // TODO create middleware
	var profileForm ProfileForm
	var user User

	decoder := json.NewDecoder(io.LimitReader(r.Body, appBodySize))
	if err := decoder.Decode(&profileForm); err != nil {
		errorResponse(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = profileForm.Validate(r)
	if err != nil {
		errorResponse(w, err, http.StatusUnprocessableEntity)
		return
	}

	db := GetDB().Where("id = ?", profileForm.ID).First(&user)
	if db.RecordNotFound() {
		errorResponse(w, "Not Found", http.StatusNotFound)
		return
	}

	if user.Email != profileForm.Email {
		if err := CheckExistingEmail(profileForm.Email); err != nil {
			errorResponse(w, err.Error(), http.StatusUnprocessableEntity)
			return
		}
	}

	newPassword := user.Password
	if profileForm.Password != "" {
		hash, err := HashPassword(profileForm.Password)
		if err != nil {
			errorResponse(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		newPassword = hash
	}

	db.Model(&user).Updates(User{
		Email:     profileForm.Email,
		Fullname:  profileForm.Fullname,
		Telephone: profileForm.Telephone,
		Address:   profileForm.Address,
		Password:  newPassword,
	})

	if db.Error != nil {
		http.Error(w, db.Error.Error(), http.StatusInternalServerError)
		return
	}

	data, _ := json.Marshal(profileForm)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
}

// CheckExistingEmail verifies if a given email is already taken
func CheckExistingEmail(email string) error {
	var count int
	GetDB().Where("email = ?", email).Count(&count)

	if count == 1 {
		return fmt.Errorf("The email %s is already taken", email)
	}
	return nil
}
