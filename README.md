# Golang Test 2017

## Converts SCSS or Sass files to CSS

Install `sass` command line

```sh
# open a new tab on your shell
cd frontend

# To compress the output
sass --watch client/styles/main.scss:assets/css/main.css

# To compress the output
sass --watch frontend/client/styles/main.scss:frontend/assets/css/main.min.css --style compressed
```

## Without Docker

### Create a database

```sh
$ mysql -u root -p 
mysql> create database gotest;
```

### Run the API and frontend

```sh
# REST API
$ cd api
$ go build -o api
$ ./api

# Frontend
$ cd frontend
$ go build -o frontend
$ ./frontend
```

## With Docker

### Run the application

```sh
$ docker-compose build
$ docker-compose up
```

### Create a database

```sh
$ docker exec -it go-test-db bash
root@c9f859f698f3:/# mysql -u root -p
mysql> create database gotest;
```

> The API is running on [localhost:8080](http://localhost:8080) and the frontend on [localhost:3000](http://localhost:3000)

