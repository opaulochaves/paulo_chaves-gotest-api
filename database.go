package main

import (
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var DB *gorm.DB
var err error

type User struct {
	gorm.Model                      // TODO remove CreatedAt, UpdatedAt, DeletedAt from json response
	Email                string     `gorm:"type:varchar(100);not null;unique_index" json:"email"`
	Password             string     `json:"password"`
	PasswordResetExpires *time.Time `json:"-"`
	PasswordResetToken   string     `json:"-"`
	Google               string     `json:"google"` // keeps the token from Google
	Fullname             string     `json:"fullname"`
	Address              string     `json:"address"`
	Telephone            string     `json:"telephone"`
}

func InitDB() (*gorm.DB, error) {
	// set up DB connection and then attempt to connect 5 times over 25 seconds
	for i := 0; i < 5; i++ {
		DB, err = gorm.Open("mysql", appDBDSN) // gorm checks Ping on Open
		if err == nil {
			break
		}
		time.Sleep(5 * time.Second)
	}

	if err != nil {
		return DB, err
	}

	// Automatically migrate your schemas
	DB.AutoMigrate(&User{})

	return DB, err
}

func CloseDB() {
	DB.Close()
}

func GetDB() *gorm.DB {
	return DB
}
